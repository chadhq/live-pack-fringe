(live-add-pack-lib "emacs-git-gutter-fringe")
(require 'git-gutter-fringe)

(setq git-gutter-fr:side 'left-fringe)

(set-face-foreground 'git-gutter-fr:modified "yellow")
(set-face-foreground 'git-gutter-fr:added    "blue")
(set-face-foreground 'git-gutter-fr:deleted  "white")

 (fringe-helper-define 'git-gutter-fr:added nil
   "........"
   "..XXXX.."
   "..XXXX.."
   "..XXXX.."
   "..XXXX.."
   "..XXXX.."
   "..XXXX.."
   "........")
 (fringe-helper-define 'git-gutter-fr:modified nil
   "........"
   "..XXXX.."
   "..XXXX.."
   "..XXXX.."
   "..XXXX.."
   "..XXXX.."
   "..XXXX.."
   "........")
 (fringe-helper-define 'git-gutter-fr:deleted nil
   "........"
   "..XXXX.."
   "..XXXX.."
   "..XXXX.."
   "..XXXX.."
   "..XXXX.."
   "..XXXX.."
   "........")
